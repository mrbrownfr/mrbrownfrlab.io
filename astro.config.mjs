import { defineConfig } from 'astro/config';

// https://astro.build/config
export default defineConfig({
    outDir: './public',
    publicDir: './assets',
    site: 'https://mrbrownfr.gitlab.io',
    sitemap: true,
    markdown: {
        drafts: true,
    },
});
